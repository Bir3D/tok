﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextField : MonoBehaviour {

    public Text nameOutput;

    public void Text_Changed(string newName)
    {
        nameOutput.text = newName;
    }
}
