﻿using UnityEngine;
using System.Collections;
public class CameraOperator : MonoBehaviour
{
    public RectTransform selectionBox;
    public static Rect selection = new Rect(0, 0, 0, 0);
    private Vector3 startClick = -Vector3.one;
    private void Update()
    { CheckCamera();
    }
    private void CheckCamera()
    {
        if (Input.GetMouseButtonDown(0))
        {
            startClick = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            startClick = -Vector3.one;
            selectionBox.sizeDelta = new Vector2(0,0);
            selectionBox.position = new Vector2(0,0);
        }
        if (Input.GetMouseButton(0))
        {
            selection = new Rect(startClick.x, InvertMouseY(startClick.y), Input.mousePosition.x - startClick.x, InvertMouseY(Input.mousePosition.y) - InvertMouseY(startClick.y));
            if (selection.width < 0)
            {
                selection.x = Input.mousePosition.x;
                selection.width = -selection.width;
            }
            if (selection.height < 0)
            {
                selection.y += selection.height;
                selection.height = -selection.height;
            }
            selectionBox.position = new Vector2(startClick.x + (Input.mousePosition.x-startClick.x) / 2, InvertMouseY(selection.y + selection.height / 2));
            selectionBox.sizeDelta = new Vector3(selection.width, selection.height);
        }
    }
    public static float InvertMouseY(float y)
    {
        return Screen.height - y;
    }
}
