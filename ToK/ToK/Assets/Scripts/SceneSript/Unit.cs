﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour
{
    public bool selected = false;
    Renderer rend;
    Color[] oldColors = new Color[20];
    
    void Start()
    {
        rend = GetComponent<Renderer>();
        for (int i = 0; i < rend.materials.Length; i++)
            oldColors[i] = rend.materials[i].color;
    }
    void Update()
    {
        rend = GetComponent<Renderer>();
        if (GetComponent<Renderer>().isVisible && Input.GetMouseButtonUp(0))
        {
            Vector3 camPos = Camera.main.WorldToScreenPoint(transform.position);
            camPos.y = CameraOperator.InvertMouseY(camPos.y);
            selected = CameraOperator.selection.Contains(camPos);
        }

        if (selected)
        {
            for (int i = 0; i < rend.materials.Length; i++)
            {
                rend.materials[i].color = Color.red;
            }

        }
        else
        {
            for (int i = 0; i < rend.materials.Length; i++)
            {
                rend.materials[i].color = oldColors[i];
            }
        }
    }

}
