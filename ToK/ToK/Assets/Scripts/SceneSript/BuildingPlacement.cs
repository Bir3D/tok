﻿using UnityEngine;
using System.Collections;

public class BuildingPlacement : MonoBehaviour {

    private PlaceableBuilding placeableBuilding;
    private Transform currentBuilding;
    private bool hasPlaced;

    // Use this for initialization
    void Start () {
    }
    
    // Update is called once per frame
    void Update() {

        Vector3 m = Input.mousePosition;
        m = new Vector3(m.x, m.y, transform.position.y);
        

        if (currentBuilding != null && !hasPlaced) {

            currentBuilding.position = new Vector3(p.x, 0, p.z);

            if (Input.GetMouseButtonDown(0)) {
                if (IsLegalPosition()) {
                    hasPlaced = true;
                }
            }
        }
    }



    bool IsLegalPosition() {
        if (placeableBuilding.colliders.Count > 0) {
            return false;
        }
        return true;
    }

    public void SetItem(GameObject b)
    {
        hasPlaced = false;
        currentBuilding = ((GameObject)Instantiate(b)).transform;
        placeableBuilding = currentBuilding.GetComponent<PlaceableBuilding>();
    }
}


Vector3 p = GetComponent<Camera>().ScreenToWorldPoint(m);