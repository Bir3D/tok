﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlaceableBuilding : MonoBehaviour {
    [HideInInspector]
    public List<Collider> colliders = new List<Collider>();

	void OnTriggerEnter(Collider c) {
		if (c.tag == "Building") {
			colliders.Add(c);	
		}
	}
	
	void OnTriggerExit(Collider c) {
		if (c.tag == "Building") {
			colliders.Remove(c);	
		}
	}
}
